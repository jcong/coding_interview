#!/usr/bin/env python2

from utils.LinkedListUtil import RandomListNode, generateRandomList, displayRandomList

class Solution:
    def Clone(self, pHead):
        if not pHead:
            return None
        curNode = pHead
        while curNode:
            cloneNode = RandomListNode(curNode.label)
            cloneNode.next = curNode.next
            curNode.next = cloneNode
            curNode = cloneNode.next
        curNode = pHead
        while curNode:
            cloneNode = curNode.next
            if curNode.random:
                cloneNode.random = curNode.random.next
            curNode = cloneNode.next
        curNode = pHead
        cloneHead = pHead.next
        while curNode:
            cloneNode = curNode.next
            if cloneNode.next:
                curNext = cloneNode.next
                cloneNext = curNext.next
                curNode.next = curNext
                cloneNode.next = cloneNext
            else:
                curNode.next = None
                cloneNode.next = None
            curNode = curNode.next
        return cloneHead


if __name__ == "__main__":
    s = Solution()

    null = -1
    nodes = [1, 2, 3, 4, 5, 6, 7, 8]
    rands = [2, 5, 2, null, 6, 2, 3, 4]

    randHead = generateRandomList(nodes, rands)
    cloneHead = s.Clone(randHead)
    
    print "original list: ",
    displayRandomList(randHead)
    print "cloned list:   ",
    displayRandomList(cloneHead)