#!/usr/bin/env python2

from utils.LinkedListUtil import ListNode, generateList


class Solution:
    def FindKthToTail(self, head, k):
        l = head
        r = l
        for i in range(k):
            if not r:
                return None
            r = r.next
        while r:
            r = r.next
            l = l.next
        return l


if __name__ == "__main__":
    s = Solution()
    nodes = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    head = generateList(nodes)
    find = s.FindKthToTail(head, 3)

    print find.val
