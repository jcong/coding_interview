class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

def generateList(nodes):
    realHead = ListNode(-1)
    curNode = realHead
    if nodes:
        for n in nodes:
            node = ListNode(n)
            curNode.next = node
            curNode = node
    return realHead.next


def displayList(list):
    curNode = list
    while curNode:
        print curNode.val, "->",
        curNode = curNode.next
    print "None"


class RandomListNode:
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None


def generateRandomList(nodes, rands):
    realHead = RandomListNode(-1)
    curNode = realHead
    if nodes:
        for n in nodes:
            node = RandomListNode(n)
            curNode.next = node
            curNode = node
    curNode = realHead.next
    if not curNode:
        return None
    randNode = realHead.next
    for r in rands:
        for i in range(0, r, 1):
            randNode = randNode.next
            if not randNode:
                print "Error: invalid random index!"
                return None
        if r == -1:
            randNode = None
        curNode.random = randNode
        curNode = curNode.next
        randNode = realHead.next
    return realHead.next


def displayRandomList(head):
    node = head
    while node:
        print node.label, "(",
        if not node.random:
            print "null ) ->",
        else:
            index = 0
            p = head
            while p and p != node.random:
                p = p.next
                index += 1
            print index, ") ->",
        node = node.next
    print "nullptr"