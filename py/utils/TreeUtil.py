class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


def generateTree(nodes, null=-1):
    if (nodes is None) or (nodes[0] == null):
        return None
    rootNode = TreeNode(nodes[0])
    queue = [rootNode]
    for i in range(1, len(nodes), 2):
        if queue is None:
            break
        leftVal = nodes[i]
        rightVal = null
        if (i + 1 < len(nodes)):
            rightVal = nodes[i + 1]
        parentNode = queue.pop(0)
        leftNode = None
        rightNode =  None
        if  leftVal != null:
            leftNode = TreeNode(leftVal)
            queue += [leftNode]
        if rightVal != null:
            rightNode = TreeNode(rightVal)
            queue += [rightNode]
        parentNode.left = leftNode
        parentNode.right = rightNode
    return rootNode


def displayTreeByLevel(root):
    print "<level>[",
    queue = [root]
    while queue:
        node = queue.pop(0)
        if node:
            print node.val,
            queue += [node.left]
            queue += [node.right]
        else:
            print "null",
    print "]"