#!/usr/bin/env python2


class Solution:
    def rectCover(self, number):
        if number < 1:
            return 0
        if number == 1:
            return 1
        methods1 = 1
        methods2 = 1
        for i in range(1, number):
            methods1, methods2 = methods2, methods1 + methods2
        return methods2


if __name__ == "__main__":
    solution = Solution()

    for i in range(10):
        print "2 x", i, "-", solution.rectCover(i)
