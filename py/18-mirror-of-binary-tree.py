#!/usr/bin/env python2


from utils.TreeUtil import TreeNode, generateTree, displayTreeByLevel


class Solution:
    def Mirror(self, root):
        if not root:
            return None
        p = root.left
        root.left = root.right
        root.right = p
        self.Mirror(root.left)
        self.Mirror(root.right)
        return root


if __name__ == "__main__":
    s = Solution()

    nodes = [8, 6, 10, 5, 7, 9, 11]
    root = generateTree(nodes)

    mirror = s.Mirror(root)
    displayTreeByLevel(mirror)
