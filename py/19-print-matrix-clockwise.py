#!/usr/bin/env python2


class Solution:
    def printMatrix(self, matrix):
        res = []
        if not matrix or not matrix[0]:
            return res
        row = len(matrix)
        col = len(matrix[0])
        rows = row
        cols = col
        start = 0
        while (2 * start < row) and (2 * start < col):
            for i in range(start, cols):
                res += [matrix[start][i]]
            for i in range(start + 1, rows):
                res += [matrix[i][cols - 1]]
            for i in range(cols - 2, start - 1, -1):
                if start < rows - 1:
                    res += [matrix[rows - 1][i]]
                else:
                    break
            for i in range(rows - 2, start, -1):
                if start < cols - 1:
                    res += [matrix[i][start]]
                else:
                    break
            start += 1
            cols -= 1
            rows -= 1
        return res


if __name__ == "__main__":
    s = Solution()
    matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    print s.printMatrix(matrix)
