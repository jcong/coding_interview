#!/usr/bin/env python

class Solution:
    def IsPopOrder(self, pushV, popV):
        stack = []
        index = 0
        size = len(pushV)
        for i in range(size):
            stack += [pushV[i]]
            while stack and stack[-1] == popV[index]:
                i ndex += 1
                stack.pop()
        return not stack

if __name__ == "__main__":
    s = Solution()

    push1 = [1, 2, 3, 4, 5]
    pop1 = [4, 5, 3, 2, 1]
    pop2 = [4, 3, 5, 1, 2]

    print s.IsPopOrder(push1, pop1)
    print s.IsPopOrder(push1, pop2)