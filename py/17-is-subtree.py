#!/usr/bin/env python2

from utils.TreeUtil import TreeNode, generateTree


class Solution:
    def HasSubtree(self, pRoot1, pRoot2):
        res = False
        if pRoot1 and pRoot2:
            if pRoot1.val == pRoot1.val:
                res = self.isSubTree(pRoot1, pRoot2)
            if not res:
                res = self.HasSubtree(pRoot1.left, pRoot2)
            if not res:
                res = self.HasSubtree(pRoot1.right, pRoot2)
        return res

    def isSubTree(self, pRoot1, pRoot2):
        if not pRoot2:
            return True
        if not pRoot1:
            return False
        if pRoot1.val != pRoot2.val:
            return False
        return self.isSubTree(pRoot1.left, pRoot2.left) and self.isSubTree(pRoot1.right, pRoot2.right)


if __name__ == "__main__":
    s = Solution()

    leaves1 = [1, 2, 3, 4, 5, 6, 7]
    leaves2 = [3, 6, 7]
    root1 = generateTree(leaves1)
    root2 = generateTree(leaves2)

    print s.HasSubtree(root1, root2)