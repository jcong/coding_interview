#!/usr/bin/env python


class Solution:
    def __init__(self):
        self.stack1 = []
        self.stack2 = []

    def push(self, node):
        self.stack1 += [node]

    def pop(self):
        if not self.stack2:
            while self.stack1:
                self.stack2 += [self.stack1.pop()]
        return self.stack2.pop()


if __name__ == "__main__":
    solution = Solution()

    solution.push(1)
    solution.push(2)
    solution.push(3)
    print solution.pop()
    solution.push(4)
    print solution.pop()
    print solution.pop()
    print solution.pop()
