#!/usr/bin/env python2


class Solution:
    def VerifySquenceOfBST(self, sequence):
        if not sequence:
            return False
        l = 0
        r = len(sequence) - 1
        while r != 0:
            while sequence[l] < sequence[r]:
                l += 1
            while sequence[l] > sequence[r]:
                l += 1
            if l < r:
                return False
            l = 0
            r -= 1
        return True


if __name__ == "__main__":
    s = Solution()
    sequence = [1, 4, 7, 6, 3, 13, 14, 10, 8]

    print s.VerifySquenceOfBST(sequence)
