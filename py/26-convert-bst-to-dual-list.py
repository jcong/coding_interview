#!/usr/bin/env python2

from utils.TreeUtil import TreeNode, generateTree


class Solution:
    def Convert(self, pRootOfTree):
        if not pRootOfTree:
            return None
        inorder = []

        def inorderTraversal(root):
            if root:
                inorderTraversal(root.left)
                inorder.append(root)
                inorderTraversal(root.right)
        inorderTraversal(pRootOfTree)
        for i, node in enumerate(inorder[:-1]):
            inorder[i].right = inorder[i+1]
            inorder[i+1].left = node
        return inorder[0]


if __name__ == "__main__":
    s = Solution()

    nodes = [50, 35, 58, 30, 40, 57, 60]
    root = generateTree(nodes)
    convert = s.Convert(root)

    def display(root):
        while(root):
            print root.val, "=",
            root = root.right
        print "None"

    display(convert)
