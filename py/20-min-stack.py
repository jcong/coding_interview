#!/usr/bin/env python2


class Solution:
    def __init__(self):
        self.mStack = []
        self.mMin = []

    def push(self, node):
        self.mStack += [node]
        if not self.mMin:
            self.mMin += [0]
        elif node < self.min():
            self.mMin += [len(self.mStack) - 1]

    def pop(self):
        self.mStack.pop()
        if len(self.mStack) == self.mMin[-1]:
            self.mMin.pop()

    def top(self):
        return self.mStack[-1]

    def min(self):
        return self.mStack[self.mMin[-1]]


if __name__ == "__main__":
    s = Solution()

    s.push(5)
    print(s.min())  # 5
    s.push(10)
    print(s.min())  # 5
    s.push(2)
    print(s.min())  # 2
    s.push(1)
    print(s.min())  # 1
    s.pop()
    print(s.min())  # 2
    s.pop()
    print(s.min())  # 5
