#!/usr/bin/env python2

from utils.LinkedListUtil import ListNode, generateList, displayList


class Solution:
    def Merge(self, pHead1, pHead2):
        realHead = ListNode(-1)
        p = realHead
        while pHead1 and pHead2:
            node = None
            if pHead1.val < pHead2.val:
                node = ListNode(pHead1.val)
                pHead1 = pHead1.next
            else:
                node = ListNode(pHead2.val)
                pHead2 = pHead2.next
            p.next = node
            p = p.next

        while pHead1:
            node = ListNode(pHead1.val)
            pHead1 = pHead1.next
            p.next = node
            p = p.next

        while pHead2:
            node = ListNode(pHead2.val)
            pHead2 = pHead2.next
            p.next = node
            p = p.next
        return realHead.next


if __name__ == "__main__":
    s = Solution()

    nodes1 = [1, 3, 5, 7, 9]
    nodes2 = [2, 4, 6, 8, 10]
    head1 = generateList(nodes1)
    head2 = generateList(nodes2)

    merged = s.Merge(head1, head2)

    displayList(merged)
