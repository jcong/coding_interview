#!/usr/bin/env python2

from utils.TreeUtil import TreeNode, generateTree


class Solution:
    def __init__(self):
        self.path = []
        self.paths = []

    def FindPath(self, root, expectNumber):
        if not root:
            return self.paths
        self.path.append(root.val)
        expectNumber -= root.val
        if not root.left and not root.right and expectNumber == 0:
            self.paths.append(self.path[:])  # copy
        if root.left:
            self.FindPath(root.left, expectNumber)
        if root.right:
            self.FindPath(root.right, expectNumber)
        v = self.path.pop()
        expectNumber += v
        return self.paths


if __name__ == "__main__":
    s = Solution()

    nodes = [1, 2, 3, 7, 5, 6, 7]
    root = generateTree(nodes)
    expect = 10

    print s.FindPath(root, expect)
