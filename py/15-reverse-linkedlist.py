#!/usr/bin/env python2

from utils.LinkedListUtil import ListNode, generateList, displayList


class Solution:
    def ReverseList(self, pHead):
        pre = None
        cur = pHead
        if not pHead:
            return None
        next = cur.next

        while next:
            cur.next = pre
            pre = cur
            cur = next
            next = next.next
        cur.next = pre
        return cur


if __name__ == "__main__":
    s = Solution()
    nodes = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    head = generateList(nodes)
    rev = s.ReverseList(head)
    displayList(rev)
