#!/usr/bin/env python2


class Solution:
    def replaceSpace(self, s):
        return s.replace(' ', '%20')


if __name__ == '__main__':
    str = "We Are Happy"
    solution = Solution()
    print solution.replaceSpace(str)
