#!/usr/bin/env python2


class Solution:
    def minNumberInRotateArray(self, rotateArray):
        length = len(rotateArray)
        if length == 0:
            return 0
        l = 0
        r = length - 1
        while r - l > 1:
            mid = (l + r) / 2
            mv = rotateArray[mid]
            if mid == 0 or mid == length - 1:
                return mv
            if mv < rotateArray[mid - 1] and mv < rotateArray[mid + 1]:
                return mv
            elif mv <= rotateArray[r]:
                r = mid
            elif mv >= rotateArray[l]:
                l = mid
        return rotateArray[r]


if __name__ == "__main__":
    solution = Solution()
    array = [3, 4, 5, 1, 2]

    print solution.minNumberInRotateArray(array)
