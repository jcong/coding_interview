#!/usr/bin/env python2


class Solution:
    def jumpFloorII(self, number):
        if number == 0:
            return 0
        jumps = 1
        for _ in range(1, number):
            jumps *= 2
        return jumps


if __name__ == "__main__":
    solution = Solution()
    print solution.jumpFloorII(10)
