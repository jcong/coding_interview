#!/usr/bin/env python2


class Solution:
    def jumpFloor(self, number):
        if number == 0:
            return 0
        jumps1 = 0
        jumps2 = 1
        for i in range(number):
            jumps1, jumps2 = jumps2, jumps1 + jumps2
        return jumps2


if __name__ == "__main__":
    solution = Solution()
    print solution.jumpFloor(2)
