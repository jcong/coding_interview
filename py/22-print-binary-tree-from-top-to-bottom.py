#!/usr/bin/env python2

from utils.TreeUtil import TreeNode, generateTree


class Solution:
    def PrintFromTopToBottom(self, root):
        res = []
        if not root:
            return res
        queue = [root]
        while queue:
            node = queue.pop(0)
            if node:
                res += [node.val]
                if node.left:
                    queue += [node.left]
                if node.right:
                    queue += [node.right]
        return res


if __name__ == "__main__":
    s = Solution()

    nodes = [1, 2, 3, 4, 5, 6, 7, 8]
    root = generateTree(nodes)

    print s.PrintFromTopToBottom(root)