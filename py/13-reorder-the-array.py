#!/usr/bin/env python2


class Solution:
    def reOrderArray(self, array):
        odd = []
        even = []
        for a in array:
            if a & 1 == 1:
                odd += [a]
            else:
                even += [a]
        array = []
        for o in odd:
            array += [o]
        for e in even:
            array += [e]
        return array


if __name__ == "__main__":
    s = Solution()
    array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    print(s.reOrderArray(array))
