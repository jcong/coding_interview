#!/usr/bin/env python2

class Solution:
    def Find(self, target, array):
        m = len(array)
        n = len(array[0])
        i = 0
        j = n - 1
        while i < m and j >= 0:
            if array[i][j] == target:
                return True
            elif array[i][j] > target:
                j -= 1
            else:
                i += 1
        return False


if __name__ == '__main__':
    array = [
        [1, 2, 8, 9],
        [2, 4, 9, 12],
        [4, 7, 10, 13],
        [6, 8, 11, 15]
    ]
    target = 7

    solution = Solution()
    print solution.Find(target, array)
