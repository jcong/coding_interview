#!/usr/bin/env python2


class Solution:
    def NumberOf1(self, n):
        cnt = 0
        if n < 0:
            n = n & 0xffffffff
        while n != 0:
            cnt += 1
            n &= (n-1)
        return cnt


if __name__ == "__main__":
    solution = Solution()
    for i in range(10):
        print solution.NumberOf1(i), "is in number", bin(i)
