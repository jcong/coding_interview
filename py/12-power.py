#!/usr/bin/env python2


class Solution:
    def Power(self, base, exponent):
        if exponent == 0:
            return 1
        negative = False
        if exponent < 0:
            negative = True
            exponent = -exponent
        p = base
        e = exponent % 2
        while exponent // 2 != 0:
            exponent = exponent // 2
            p *= p
        if e == 1:
            p *= base
        if negative:
            p = 1.0 / p
        return p


if __name__ == "__main__":
    s = Solution()

    base = 1.1
    for i in range(-5, 5):
        print base, '^', i, '=', s.Power(base, i)
