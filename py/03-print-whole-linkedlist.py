#!/usr/bin/env python2

from utils.LinkedListUtil import generateList

class Solution:
    def printListFromTailToHead(self, listNode):
        if listNode is None:
            return []
        return self.printListFromTailToHead(listNode.next) + [listNode.val]

if __name__ == "__main__":
    nodes = [1, 3, 5, 7, 9, 0, 8, 6, 4, 2]
    lst = generateList(nodes)

    solution = Solution()
    print solution.printListFromTailToHead(lst)