#!/usr/bin/env python2


class Solution:
    def Fibonacci(self, n):
        if n < 1:
            return 0
        if n < 3:
            return 1
        fib1 = 1
        fib2 = 1
        for i in range(3, n + 1):
            fib1, fib2 = fib2, fib2 + fib1
        return fib2


if __name__ == "__main__":
    solution = Solution()
    print solution.Fibonacci(5)
