/**
 * 输入一个链表，反转链表后，输出新链表的表头。
 */

#include "../utils/LinkedListUitl.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::ListNode;

class Solution {
public:
    ListNode *ReverseList(ListNode *pHead) {
        ListNode *pre = nullptr, *cur = pHead, *next = nullptr;
        if (!pHead) return nullptr;
        next = cur->next;
        while (next) {
            cur->next = pre;
            pre = cur;
            cur = next;
            next = next->next;
        }
        cur->next = pre;
        return cur;
    }
};

int main() {
    Solution s;
    vector<int> nodes = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    auto *head = utils::generateLinkedList(nodes);
    auto *rev = s.ReverseList(head);

    utils::displayLinkedList(rev);
    utils::releaseLinkedList(rev);
}