/**
 * 操作给定的二叉树，将其变换为源二叉树的镜像。
 * 二叉树的镜像定义：
 * 源二叉树
 *           8
 *         /  \
 *        6   10
 *       / \  / \
 *      5  7 9 11
 * 镜像二叉树
 *          8
 *         /  \
 *        10   6
 *       / \  / \
 *      11 9 7  5
 */

#include "../utils/TreeUtil.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::TreeNode;

class Solution {
public:
    void Mirror(TreeNode *pRoot) {
        if (!pRoot) return;
        auto *p = pRoot->left;
        pRoot->left = pRoot->right;
        pRoot->right = p;
        Mirror(pRoot->left);
        Mirror(pRoot->right);
    }
};

int main() {
    Solution s;

    vector<int> nodes{8, 6, 10, 5, 7, 9, 11};
    auto *root = utils::generateTree(nodes);

    s.Mirror(root);
    utils::displayTreeByLevel(root);

    utils::releaseTree(root);
}