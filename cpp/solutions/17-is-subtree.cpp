/**
 * 输入两棵二叉树A，B，判断B是不是A的子结构。（ps：我们约定空树不是任意一个树的子结构）
 */

#include "../utils/TreeUtil.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::TreeNode;

class Solution {
public:
    bool HasSubtree(TreeNode *pRoot1, TreeNode *pRoot2 /* sub */) {
        bool res = false;
        if (pRoot1 && pRoot2) {
            if (pRoot1->val == pRoot2->val) res = isSubTree(pRoot1, pRoot2);
            if (!res) res = HasSubtree(pRoot1->left, pRoot2);
            if (!res) res = HasSubtree(pRoot1->right, pRoot2);
        }
        return res;
    }

    bool isSubTree(TreeNode *pRoot1, TreeNode *pRoot2 /* sub */) {
        if (!pRoot2) return true;
        if (!pRoot1) return false;
        if (pRoot1->val != pRoot2->val) return false;
        return isSubTree(pRoot1->left, pRoot2->left) && isSubTree(pRoot1->right, pRoot2->right);
    }
};

int main() {
    Solution s;

    constexpr int null = -1;
    vector<int> leaves1{1, 2, 3, 4, 5, 6, 7};
    vector<int> leaves2{3, 6, 7};

    auto *root1 = utils::generateTree(leaves1);
    auto *root2 = utils::generateTree(leaves2);

    std::cout << std::boolalpha << s.HasSubtree(root1, root2) << std::endl;

    utils::releaseTree(root1);
    utils::releaseTree(root2);
}