/**
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个n级的台阶总共有多少种跳法（先后次序不同算不同
 * 的结果）。
 */

#include <iostream>

class Solution {
public:
    int jumpFloor(int number) {
        if (number == 0) return 0;
        int jumps1 = 0, jumps2 = 1;
        for (int i = 1; i <= number; ++i) {
            int jumps = jumps1;
            jumps1 = jumps2;
            jumps2 += jumps;
        }
        return jumps2;
    }
};

int main() {
    Solution solution;
    std::cout << solution.jumpFloor(2) << std::endl;
}