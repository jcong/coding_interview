/**
 * 给定一个double类型的浮点数base和int类型的整数exponent。求base的exponent次方。
 */

#include <iostream>

class Solution {
public:
    double Power(double base, int exponent) {
        if (exponent == 0) return 1;
        bool negative = exponent < 0;
        double p = base;
        int e = exponent % 2;
        while (exponent / 2) {
            exponent /= 2;
            p *= p;
        }
        if (e) p *= base;
        if (negative) p = 1.0 / p;
        return p;
    }
};

int main() {
    Solution s;
    float base = 1.1;
    for (int i = -5; i < 5; ++i) {
        std::cout << base << "^" << i << " = " << s.Power(base, i) << std::endl;
    }
}