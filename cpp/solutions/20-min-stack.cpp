/**
 * 定义栈的数据结构，请在该类型中实现一个能够得到栈中所含最小元素的min函数（时间复杂度应为O（1））。
 */

#include <iostream>
#include <stack>
#include <vector>

using std::stack;
using std::vector;

class Solution {
public:
    void push(int value) {
        mStack.emplace_back(value);
        if (mMinIndex.empty()) {
            mMinIndex.emplace_back(0);
        } else if (value < min()) {
            mMinIndex.emplace_back(mStack.size() - 1);
        }
    }
    void pop() {
        mStack.pop_back();
        if (mStack.size() == mMinIndex.back()) mMinIndex.pop_back();
    }
    int top() { return mStack.back(); }
    int min() { return mStack[mMinIndex.back()]; }

private:
    vector<int> mStack;
    vector<int> mMinIndex;
};

int main() {
    Solution s;

    auto min = [&]() { std::cout << s.min() << std::endl; };

    s.push(5);
    min();  // 5
    s.push(10);
    min();  // 5
    s.push(2);
    min();  // 2
    s.push(1);
    min();  // 1
    s.pop();
    min();  // 2
    s.pop();
    min();  // 5
}