/**
 * 输入一个字符串,按字典序打印出该字符串中字符的所有排列。例如输入字符串abc,则打印出由字符a,b,c所能排列出来
 * 的所有字符串abc,acb,bac,bca,cab和cba。
 */

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using std::sort;
using std::string;
using std::vector;

class Solution {
public:
    vector<string> Permutation(string str) {
        vector<string> res;
        if (str.length() == 0) return res;
        // sort to make str in lexicographical order
        sort(str.begin(), str.end());
        res.emplace_back(str);
        while (nextPermute(str)) { res.emplace_back(str); }
        return res;
    }

    bool nextPermute(string &str) {
        int end = str.length() - 1;
        // checking from tail to ensure in lexicographical order
        for (int idx = end; idx > 0; --idx) {
            if (str[idx] > str[idx - 1]) {
                int i = end;
                while (str[i] <= str[idx - 1]) --i;
                char c = str[i];
                str[i] = str[idx - 1];
                str[idx - 1] = c;
                int l = idx, r = end;
                while (l < r) {
                    char t = str[l];
                    str[l] = str[r];
                    str[r] = t;
                    ++l;
                    --r;
                }
                return true;
            }
        }
        return false;
    }
};

int main() {
    Solution s;

    string str{"abcdef"};

    auto res = s.Permutation(str);
    for (const auto &s : res) { std::cout << s << ", "; }
    std::cout << std::endl;
}