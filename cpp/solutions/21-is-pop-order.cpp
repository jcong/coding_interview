/**
 * 输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否可能为该栈的弹出顺序。假设压入栈的所有数
 * 字均不相等。例如序列1,2,3,4,5是某栈的压入顺序，序列4,5,3,2,1是该压栈序列对应的一个弹出序列，但4,3,5,1,2
 * 就不可能是该压栈序列的弹出序列。（注意：这两个序列的长度是相等的）
 */

#include <iostream>
#include <stack>
#include <vector>

using std::stack;
using std::vector;

class Solution {
public:
    bool IsPopOrder(vector<int> pushV, vector<int> popV) {
        stack<int> stk;
        // trust that pushV & popV have same size
        int ipush = 0, ipop = 0;
        for (; ipush < pushV.size(); ++ipush) {
            stk.emplace(pushV[ipush]);
            while (!stk.empty() && stk.top() == popV[ipop]) {
                stk.pop();
                ++ipop;
            }
        }
        return stk.empty();
    }
};

int main() {
    Solution s;

    vector<int> pushV1{1, 2, 3, 4, 5};
    vector<int> popV1{4, 5, 3, 2, 1};
    vector<int> popV2{4, 3, 5, 1, 2};

    std::cout << std::boolalpha << s.IsPopOrder(popV1, popV1) << std::endl
              << s.IsPopOrder(pushV1, popV2) << std::endl;
}