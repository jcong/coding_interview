/**
 * 输入一个链表，按链表值从尾到头的顺序返回一个ArrayList。
 */

#include "../utils/LinkedListUitl.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::ListNode;

class Solution {
public:
    vector<int> printListFromTailToHead(ListNode* head) {
        if (head != nullptr) {
            printListFromTailToHead(head->next);
            arrayList.emplace_back(head->val);
        }
        return arrayList;
    }

    vector<int> arrayList;
};

int main() {
    Solution solution;

    vector<int> vec{1, 3, 5, 7, 9, 0, 8, 6, 4, 2};
    auto* head = utils::generateLinkedList(vec);

    auto res = solution.printListFromTailToHead(head);
    for (const auto& e : res) {
        std::cout << e << ", ";
    }
    std::cout << std::endl;

    utils::releaseLinkedList(head);
}