/**
 * 从上往下打印出二叉树的每个节点，同层节点从左至右打印。
 */

#include "../utils/TreeUtil.hpp"

#include <iostream>
#include <queue>
#include <vector>

using std::queue;
using std::vector;
using utils::TreeNode;

class Solution {
public:
    // traverse by level
    vector<int> PrintFromTopToBottom(TreeNode *root) {
        vector<int> res;
        if (!root) return res;
        queue<TreeNode *> nodeQueue;
        nodeQueue.emplace(root);
        while (!nodeQueue.empty()) {
            auto *node = nodeQueue.front();
            nodeQueue.pop();
            if (node) {
                res.emplace_back(node->val);
                if (node->left) nodeQueue.emplace(node->left);
                if (node->right) nodeQueue.emplace(node->right);
            }
        }
        return res;
    }
};

int main() {
    Solution s;

    vector<int> nodes = {1, 2, 3, 4, 5, 6, 7, 8};
    auto *root = utils::generateTree(nodes);

    auto traversal = s.PrintFromTopToBottom(root);

    for (const auto &t : traversal) std::cout << t << ", ";
    std::cout << std::endl;

    utils::releaseTree(root);
}