/**
 * 输入一个整数，输出该数二进制表示中1的个数。其中负数用补码表示。
 */

#include <bitset>
#include <iostream>

class Solution {
public:
    int NumberOf1(int n) {
        int cnt = 0;
        while (n != 0) {
            ++cnt;
            n &= (n - 1);
        }
        return cnt;
    }
};

int main() {
    Solution s;
    for (int i = 0; i < 10; ++i)
        std::cout << s.NumberOf1(i) << " 1s in number "
                  << std::bitset<sizeof(int) * 8>(i) << std::endl;
}