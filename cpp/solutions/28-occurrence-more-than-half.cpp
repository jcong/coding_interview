/**
 * 数组中有一个数字出现的次数超过数组长度的一半，请找出这个数字。例如输入一个长度为9的数组{1,2,3,2,2,2,5,4,2}。
 * 由于数字2在数组中出现了5次，超过数组长度的一半，因此输出2。如果不存在则输出0。
 */

#include <iostream>
#include <vector>

using std::vector;

class Solution {
public:
    int MoreThanHalfNum_Solution(vector<int> numbers) {
        if (numbers.empty()) return 0;
        auto size = numbers.size();
        int number = numbers[0];
        int occurance = 1;
        for (auto i = 1; i < size; ++i) {
            if (number == numbers[i])
                ++occurance;
            else
                --occurance;
            if (occurance == 0) {
                number = numbers[i];
                occurance = 1;
            }
        }
        occurance = 0;
        for (const auto &n : numbers) {
            if (n == number) ++occurance;
        }
        return (2 * occurance > size) ? number : 0;
    }
};

int main() {
    Solution s;

    vector<int> number{1, 2, 3, 2, 2, 2, 5, 4, 2};

    std::cout << s.MoreThanHalfNum_Solution(number) << std::endl;
}