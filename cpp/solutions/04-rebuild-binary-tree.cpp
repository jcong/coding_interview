/**
 * 输入某二叉树的前序遍历和中序遍历的结果，请重建出该二叉树。假设输入的前序遍历和中序遍历的结果中都不含重复的
 * 数字。例如输入前序遍历序列{1,2,4,7,3,5,6,8}和中序遍历序列{4,7,2,1,5,3,8,6}，则重建二叉树并返回。
 */

#include "../utils/TreeUtil.hpp"

#include <vector>

using std::vector;
using utils::TreeNode;

class Solution {
public:
    TreeNode *reConstructBinaryTree(vector<int> pre, vector<int> vin) {
        int index = 0;
        return rebuild(pre, index, vin, 0, vin.size() - 1);
    }

    TreeNode *rebuild(const vector<int> &pre, int &index, const vector<int> &in,
                      int inLeft, int inRight) {
        if (inLeft > inRight || index >= pre.size()) return nullptr;
        auto *root = new TreeNode(pre[index]);
        int i = inLeft;
        while (i < inRight && in[i] != pre[index]) ++i;
        ++index;
        root->left = rebuild(pre, index, in, inLeft, i - 1);
        root->right = rebuild(pre, index, in, i + 1, inRight);
        return root;
    }
};

int main() {
    Solution solution;

    vector<int> preorder{1, 2, 4, 7, 3, 5, 6, 8};
    vector<int> inorder{4, 7, 2, 1, 5, 3, 8, 6};

    auto *root = solution.reConstructBinaryTree(preorder, inorder);

    utils::displayTreeByLevel(root);
    utils::releaseTree(root);
}