/**
 * 输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历的结果。如果是则输出Yes,否则输出No。假设输入的数组
 * 的任意两个数字都互不相同。
 */

#include <iostream>
#include <vector>

using std::vector;

class Solution {
public:
    bool VerifySquenceOfBST(vector<int> sequence) {
        if (sequence.empty()) return false;
        int l = 0, r = sequence.size();
        while (--r) {
            while (sequence[l++] < sequence[r]);
            while (sequence[l++] > sequence[r]);
            if (l < r) return false;
            l = 0;
        }
        return true;
    }
};

int main() {
    Solution s;
    vector<int> sequence{4, 8, 6, 12, 16, 14, 10};

    std::cout << std::boolalpha;
    std::cout << s.VerifySquenceOfBST(sequence) << std::endl;
}