/**
 * 输入一个复杂链表（每个节点中有节点值，以及两个指针，一个指向下一个节点，另一个特殊指针指向任意一个节点），
 * 返回结果为复制后复杂链表的head。（注意，输出结果中请不要返回参数中的节点引用，否则判题程序会直接返回空）
 */

#include "../utils/LinkedListUitl.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::RandomListNode;

static constexpr int null = -1;

class Solution {
public:
    RandomListNode *Clone(RandomListNode *pHead) {
        if (!pHead) return nullptr;
        auto *pNode = pHead;
        // create cloned list in original list
        while (pNode) {
            auto *pCloneNode = new RandomListNode(pNode->label);
            pCloneNode->next = pNode->next;
            pNode->next = pCloneNode;
            pNode = pCloneNode->next;
        }
        // clone randoms ptrs
        pNode = pHead;
        while (pNode) {
            auto *pCloneNode = pNode->next;
            if (pNode->random) pCloneNode->random = pNode->random->next;
            pNode = pCloneNode->next;
        }
        // split
        pNode = pHead;
        auto pCloneHead = pHead->next;
        while (pNode) {
            auto *pCloneNode = pNode->next;
            if (pCloneNode->next) {
                auto *pNext = pCloneNode->next;
                auto *pCloneNext = pNext->next;
                pNode->next = pNext;
                pCloneNode->next = pCloneNext;
            } else {
                pNode->next = nullptr;
                pCloneNode->next = nullptr;
            }
            pNode = pNode->next;
        }

        return pCloneHead;
    }
};

int main() {
    Solution s;

    vector<int> nodes{1, 2, 3, 4, 5, 6, 7, 8};
    vector<int> rands{2, 5, 2, null, 6, 2, 3, 4};

    auto *randHead = utils::generateRandomList(nodes, rands);

    auto *cloned = s.Clone(randHead);

    std::cout << "original list: ";
    utils::displayRandomList(randHead);
    utils::releaseRandomList(randHead);
    std::cout << "original list released." << std::endl;

    std::cout << "cloned list:   ";
    utils::displayRandomList(cloned);
    utils::releaseRandomList(cloned);
    std::cout << "cloned list released." << std::endl;
}