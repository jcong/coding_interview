/**
 * 用两个栈来实现一个队列，完成队列的Push和Pop操作。 队列中的元素为int类型。
 */

#include <iostream>
#include <stack>

using std::stack;

class Solution {
public:
    void push(int node) { stack1.emplace(node); }

    int pop() {
        if (stack2.empty()) {
            while (!stack1.empty()) {
                stack2.emplace(stack1.top());
                stack1.pop();
            }
        }
        auto v = stack2.top();
        stack2.pop();
        return v;
    }

private:
    stack<int> stack1;
    stack<int> stack2;
};

int main() {
    Solution solution;
    solution.push(1);
    solution.push(2);
    solution.push(3);

    std::cout << solution.pop() << std::endl;

    solution.push(4);

    std::cout << solution.pop() << std::endl;
    std::cout << solution.pop() << std::endl;
    std::cout << solution.pop() << std::endl;
}