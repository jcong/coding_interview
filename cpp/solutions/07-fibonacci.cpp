/**
 *大家都知道斐波那契数列，现在要求输入一个整数n，请你输出斐波那契数列的第n项（从0开始，第0项为0）。n<=39
 */

#include <iostream>

class Solution {
public:
    int Fibonacci(int n) {
        if (n < 1) return 0;
        if (n <= 2) return 1;
        int fib1 = 1, fib2 = 1;
        for (int i = 3; i <= n; ++i) {
            auto f = fib1;
            fib1 = fib2;
            fib2 += f;
        }
        return fib2;
    }
};

int main() {
    Solution s;
    std::cout << s.Fibonacci(5) << std::endl;
}