/**
 * 输入一棵二叉搜索树，将该二叉搜索树转换成一个排序的双向链表。要求不能创建任何新的结点，
 * 只能调整树中结点指针的指向。
 */

#include "../utils/TreeUtil.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::TreeNode;

class Solution {
public:
    TreeNode *Convert(TreeNode *pRootOfTree) {
        if (!pRootOfTree) return nullptr;
        TreeNode *pre = nullptr;
        ConvertInorder(pRootOfTree, &pre);
        auto *head = pre;
        while (head->left) { head = head->left; }
        return head;
    }

    void ConvertInorder(TreeNode *cur, TreeNode **pre) {
        if (!cur) return;
        ConvertInorder(cur->left, pre);
        cur->left = *pre;
        if (*pre) (*pre)->right = cur;
        *pre = cur;
        ConvertInorder(cur->right, pre);
    }
};

int main() {
    Solution s;

    /**
     * BST:
     *       50
     *     /    \
     *   35      58
     *  /  \    /  \
     * 30  40  57  60
     *
     * Dual List:
     * 30 = 35 = 40 = 50 = 57 = 58 = 60
     */
    vector<int> nodes{50, 35, 58, 30, 40, 57, 60};
    auto *root = utils::generateTree(nodes);
    auto *convert = s.Convert(root);

    auto DisplayAndRelease = [](TreeNode *head) {
        while (head) {
            std::cout << head->val << " = ";
            auto *next = head->right;
            delete head;
            head = next;
        }
        std::cout << "nullptr" << std::endl;
    };
    DisplayAndRelease(convert);
}