/**
 * 输入一颗二叉树的跟节点和一个整数，打印出二叉树中结点值的和为输入整数的所有路径。路径定义为从树的根结点开始
 * 往下一直到叶结点所经过的结点形成一条路径。(注意:
 * 在返回值的list中，数组长度大的数组靠前)
 */

#include "../utils/TreeUtil.hpp"

#include <iostream>
#include <stack>
#include <vector>

using std::stack;
using std::vector;
using utils::TreeNode;

static constexpr int null = -1;

#define COMPILE_VERSION 2

#if (COMPILE_VERSION == 1)
class Solution {
public:
    vector<vector<int>> FindPath(TreeNode *root, int expectNumber) {
        vector<vector<int>> paths;
        if (!root) return paths;
        vector<int> path;
        int sum = 0;
        find(root, sum, expectNumber, path, paths);
        return paths;
    }

    void find(TreeNode *root, int sum, int expect, vector<int> &path, vector<vector<int>> &paths) {
        path.emplace_back(root->val);
        sum += root->val;
        if (!root->left && !root->right && sum == expect) { paths.emplace_back(path); }
        if (root->left) find(root->left, sum, expect, path, paths);
        if (root->right) find(root->right, sum, expect, path, paths);
        path.pop_back();
    }
};
#elif (COMPILE_VERSION == 2)
class Solution {
public:
    vector<vector<int>> FindPath(TreeNode *root, int expectNumber) {
        vector<vector<int>> paths;
        if (!root) return paths;
        vector<int> path;
        stack<TreeNode *> nodeStack;
        TreeNode *node = root, *pre = nullptr;
        while (node || !nodeStack.empty()) {
            while (node) {
                path.emplace_back(node->val);
                nodeStack.emplace(node);
                expectNumber -= node->val;
                node = node->left;
            }
            node = nodeStack.top();
            if (!node->left && !node->right && !expectNumber) paths.emplace_back(path);
            if (node->right && pre != node->right)
                node = node->right;
            else {
                nodeStack.pop();
                expectNumber += node->val;
                path.pop_back();
                pre = node;
                node = nullptr;
            }
        }
        return paths;
    }
};
#endif

int main() {
    Solution s;

    vector<int> nodes{1, 2, 3, 7, 5, 6, 7};
    int expect = 10;

    auto *root = utils::generateTree(nodes);

    auto path = s.FindPath(root, expect);

    for (const auto &p : path) {
        for (const auto &e : p) { std::cout << e << ", "; }
        std::cout << std::endl;
    }

    utils::releaseTree(root);
}