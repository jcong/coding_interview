/**
 * 输入一个矩阵，按照从外向里以顺时针的顺序依次打印出每一个数字，例如，如果输入如下4
 * X 4矩阵： 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
 * 则依次打印出数字1,2,3,4,8,12,16,15,14,13,9,5,6,7,11,10.
 */

#include <iostream>
#include <vector>

using std::vector;

class Solution {
public:
    vector<int> printMatrix(vector<vector<int>> matrix) {
        vector<int> res;
        if (matrix.empty() || matrix[0].empty()) return res;
        int row = matrix.size(), col = matrix[0].size();
        int rows = row, cols = col, start = 0;
        while (col > start * 2 && row > start * 2) {
            for (int i = start; i < cols; ++i)
                res.emplace_back(matrix[start][i]);
            for (int i = start + 1; i < rows; ++i)
                res.emplace_back(matrix[i][cols - 1]);
            for (int i = cols - 2; i >= start && start < rows - 1; --i)
                res.emplace_back(matrix[rows - 1][i]);
            for (int i = rows - 2; i > start && start < cols - 1; --i)
                res.emplace_back(matrix[i][start]);
            ++start;
            rows -= 1;
            cols -= 1;
        }
        return res;
    }
};

int main() {
    Solution s;

    vector<vector<int>> matrix{{1}, {2}, {3}, {4}, {5}};

    auto res = s.printMatrix(matrix);

    for (const auto &e : res) std::cout << e << ", ";
    std::cout << std::endl;
}