/**
 * 输入n个整数，找出其中最小的K个数。例如输入4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4,。
 */

#include <functional>
#include <iostream>
#include <queue>
#include <vector>

using std::greater;
using std::priority_queue;
using std::vector;

#define COMPILE_VERSION 2

class MinHeap {
public:
    MinHeap(const vector<int> &vec) {
        size = vec.size();
        heap.assign(vec.begin(), vec.end());
        for (int i = (size - 1) / 2; i >= 0; --i) shifDown(i);
    }

    int pop() {
        int top = heap[0];
        --size;
        heap[0] = heap[size];
        shifDown(0);
        return top;
    }

private:
    vector<int> heap;
    int size;

    void shifDown(int i) {
        bool conti = true;
        int t = -1;
        while (i * 2 + 2 <= size && conti) {
            if (heap[i] > heap[i * 2 + 1])
                t = i * 2 + 1;  // left
            else
                t = i;

            if (i * 2 + 2 <= size - 1 && heap[t] > heap[i * 2 + 2]) t = i * 2 + 2;

            if (t != i) {
                auto v = heap[i];
                heap[i] = heap[t];
                heap[t] = v;
                i = t;
            } else {
                conti = false;
            }
        }
    }
};

#if (COMPILE_VERSION == 1)  // use STL
class Solution {
public:
    vector<int> GetLeastNumbers_Solution(vector<int> input, int k) {
        vector<int> res;
        if (input.empty() || k == 0 || k > input.size()) return res;

        priority_queue<int, vector<int>, greater<int>> minHeap(greater<int>(), input);
        for (auto i = 0; i < k; ++i) {
            res.emplace_back(minHeap.top());
            minHeap.pop();
        }
        return res;
    }
};
#elif (COMPILE_VERSION == 2)  // use self defined heap
class Solution {
public:
    vector<int> GetLeastNumbers_Solution(vector<int> input, int k) {
        vector<int> res;
        if (input.empty() || k == 0 || k > input.size()) return res;

        MinHeap heap(input);
        for (int i = 0; i < k; ++i) res.emplace_back(heap.pop());
        return res;
    }
};
#endif

int main() {
    Solution s;

    vector<int> input{4, 5, 1, 6, 2, 7, 3, 8};
    int k = 4;

    auto res = s.GetLeastNumbers_Solution(input, k);

    for (const auto &n : res) { std::cout << n << ", "; }
    std::cout << std::endl;
}