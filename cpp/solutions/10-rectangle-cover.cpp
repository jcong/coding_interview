/**
 * 我们可以用2*1的小矩形横着或者竖着去覆盖更大的矩形。请问用n个2*1的小矩形无重叠地覆盖一个2*n的大矩形，
 * 总共有多少种方法？
 */

#include <iostream>

class Solution {
public:
    int rectCover(int number) {
        if (number < 1) return 0;
        if (number == 1) return 1;
        int methods1 = 1, methods2 = 1;
        for (int i = 2; i <= number; ++i) {
            int methods = methods1;
            methods1 = methods2;
            methods2 = methods2 + methods;
        }
        return methods2;
    }
};

int main() {
    Solution solution;

    for (int num = 0; num < 10; ++num) {
        std::cout << "2x" << num << " - " << solution.rectCover(num) << " ways"
                  << std::endl;
    }
}