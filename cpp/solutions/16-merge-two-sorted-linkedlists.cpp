/**
 * 输入两个单调递增的链表，输出两个链表合成后的链表，当然我们需要合成后的链表满足单调不减规则。
 */

#include "../utils/LinkedListUitl.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::ListNode;

class Solution {
public:
    ListNode *Merge(ListNode *pHead1, ListNode *pHead2) {
        // copy, won't affect original two lists
        ListNode *realHead = new ListNode(-1), *p = realHead;
        while (pHead1 && pHead2) {
            ListNode *node = nullptr;
            if (pHead1->val < pHead2->val) {
                node = new ListNode(pHead1->val);
                pHead1 = pHead1->next;
            } else {
                node = new ListNode(pHead2->val);
                pHead2 = pHead2->next;
            }
            p->next = node;
            p = p->next;
        }

        while (pHead1) {
            ListNode *node = new ListNode(pHead1->val);
            pHead1 = pHead1->next;
            p->next = node;
            p = p->next;
        }

        while (pHead2) {
            ListNode *node = new ListNode(pHead2->val);
            pHead2 = pHead2->next;
            p->next = node;
            p = p->next;
        }

        p = realHead->next;
        delete realHead;
        return p;
    }
};

int main() {
    Solution s;
    vector<int> nodes1{1, 3, 5, 7, 9};
    vector<int> nodes2{2, 4, 6, 8, 10};

    auto *head1 = utils::generateLinkedList(nodes1);
    auto *head2 = utils::generateLinkedList(nodes2);

    auto *merged = s.Merge(head1, head2);

    utils::displayLinkedList(merged);

    utils::releaseLinkedList(head1);
    utils::releaseLinkedList(head2);
    utils::releaseLinkedList(merged);
}