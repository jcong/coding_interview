/**
 * 输入一个链表，输出该链表中倒数第k个结点。
 */

#include "../utils/LinkedListUitl.hpp"

#include <iostream>
#include <vector>

using std::vector;
using utils::ListNode;

class Solution {
public:
    ListNode* FindKthToTail(ListNode* pListHead, unsigned int k) {
        ListNode *pl = pListHead, *pr = pl;
        while (k--) {
            if (pr == nullptr) return nullptr;
            pr = pr->next;
        }
        while (pr) {
            pr = pr->next;
            pl = pl->next;
        }
        return pl;
    }
};

int main() {
    Solution s;
    vector<int> nodes{1, 2, 3, 4, 5, 6, 7, 8, 9};
    auto* head = utils::generateLinkedList(nodes);

    auto* find = s.FindKthToTail(head, 3);
    std::cout << find->val << std::endl;

    utils::releaseLinkedList(head);
}