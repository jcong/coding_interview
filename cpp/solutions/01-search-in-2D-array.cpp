/**
 * 在一个二维数组中（每个一维数组的长度相同），每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的
 * 顺序排序。请完成一个函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 */

#include <iostream>
#include <vector>

using std::vector;

class Solution {
public:
    bool Find(int target, vector<vector<int>> array) {
        int m = array.size();
        if (m == 0) return false;
        int n = array[0].size();
        if (n == 0) return false;

        return checkTopRight(target, array, 0, n - 1, m);
    }

    bool checkTopRight(int &target, vector<vector<int>> &array, int i, int j,
                       int &m) {
        if (i >= m || j < 0) return false;
        const auto &v = array[i][j];
        if (v == target)
            return true;
        else if (v > target)
            return checkTopRight(target, array, i, j - 1, m);
        else
            return checkTopRight(target, array, i + 1, j, m);
    }
};

int main() {
    Solution solution;

    vector<vector<int>> array{
        {1, 2, 8, 9}, {2, 4, 9, 12}, {4, 7, 10, 13}, {6, 8, 11, 15}};

    int target = 7;

    auto res = solution.Find(target, array);
    std::cout << (res ? "true" : "false") << std::endl;
}