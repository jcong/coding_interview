/**
 * 请实现一个函数，将一个字符串中的每个空格替换成“%20”。例如，当字符串为We Are
 * Happy.则经过替换之后的字符串 为We%20Are%20Happy。
 */

#include <cstring>
#include <iostream>

class Solution {
public:
    /**
     * @param str       字符串
     * @param length 字符串长度（一般要比实际有效字符串长以保留处理后的字符串）
     */
    void replaceSpace(char *str, int length) {
        auto strLen = 0, spaces = 0;
        for (int i = 0; i < length; ++i) {
            if (str[i] == '\0')
                break;
            else if (str[i] == ' ')
                ++spaces;
            else
                ++strLen;
        }
        auto originalLen = strLen + spaces;
        auto processedLen = strLen + 3 * spaces;
        for (int i = processedLen /* '\0' */, j = originalLen; i >= 0;
             --i, --j) {
            if (str[j] == ' ') {
                str[i] = '0';
                str[i - 1] = '2';
                str[i - 2] = '%';
                i -= 2;
            } else {
                str[i] = str[j];
            }
        }
    }
};

int main() {
    Solution solution;

    int length = 20;
    char *str = new char[length];
    memset(str, 0, length);
    strcpy(str, " helloworld");

    auto print = [&](const char *desc) {
        std::cout << desc;
        for (int i = 0; i < length; ++i) {
            if (str[i] == '\0')
                std::cout << "\\0";
            else
                std::cout << str[i];
        }
        std::cout << std::endl;
    };

    print("original  :");
    solution.replaceSpace(str, length);
    print("processed :");
}