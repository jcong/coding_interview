/**
 * 输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前半部分，所有的偶数位于数
 * 组的后半部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变。
 */

#include <iostream>
#include <vector>

using std::vector;

class Solution {
public:
    void reOrderArray(vector<int> &array) {
        vector<int> odd, even;
        for (const auto &e : array) {
            if (e & 0x1)
                odd.emplace_back(e);
            else
                even.emplace_back(e);
        }
        int i = 0;
        for (const auto &o : odd) array[i++] = o;
        for (const auto &e : even) array[i++] = e;
    }
};

int main() {
    Solution s;
    vector<int> array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    s.reOrderArray(array);

    auto print = [](const vector<int> &vec) {
        for (const int &v : vec) std::cout << v << ", ";
        std::cout << std::endl;
    };
    print(array);
}