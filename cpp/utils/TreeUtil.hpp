#ifndef __TREE_UTIL_HPP__
#define __TREE_UTIL_HPP__

#include <iostream>
#include <queue>
#include <string>
#include <vector>

namespace utils {

using std::queue;
using std::to_string;
using std::vector;

// definition
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

template <typename T = TreeNode>
inline T *generateTree(const vector<int> &vec, const int null = -1) {
    if (vec.empty() || vec[0] == null) return nullptr;
    auto *root = new T(vec[0]);
    queue<T *> nodeQueue;
    nodeQueue.emplace(root);
    for (int i = 1; i < vec.size() && !nodeQueue.empty(); i += 2) {
        auto leftVal = vec[i], rightVal = null;
        if (i + 1 < vec.size()) rightVal = vec[i + 1];
        auto *pParent = nodeQueue.front();
        nodeQueue.pop();
        T *pLeft = nullptr, *pRight = nullptr;
        if (leftVal != null) {
            pLeft = new T(leftVal);
            nodeQueue.emplace(pLeft);
        }
        if (rightVal != null) {
            pRight = new T(rightVal);
            nodeQueue.emplace(pRight);
        }
        pParent->left = pLeft;
        pParent->right = pRight;
    }
    return root;
}

template <typename T = TreeNode>
void displayTreeByLevel(const T *root) {
    std::cout << "<level> ";
    queue<const T *> nodeQueue;
    nodeQueue.emplace(root);
    std::cout << "[";
    while (!nodeQueue.empty()) {
        auto *pNode = nodeQueue.front();
        nodeQueue.pop();
        std::cout << (pNode ? to_string(pNode->val) : "null") << ", ";
        if (pNode) {
            nodeQueue.emplace(pNode->left);
            nodeQueue.emplace(pNode->right);
        }
    }
    std::cout << "]" << std::endl;
}

template <typename T = TreeNode>
void releaseTree(T *root) {
    if (root == nullptr) return;
    auto *left = root->left, *right = root->right;
    delete root;
    releaseTree(left);
    releaseTree(right);
}

}  // namespace utils

#endif  // __TREE_UTIL_HPP__