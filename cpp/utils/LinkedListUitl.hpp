#ifndef __LINKED_LIST_UTIL_HPP__
#define __LINKED_LIST_UTIL_HPP__

#include <iostream>
#include <string>
#include <vector>

namespace utils {

using std::to_string;
using std::vector;

// definition
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};

struct RandomListNode {
    int label;
    struct RandomListNode *next, *random;
    RandomListNode(int x) : label(x), next(nullptr), random(nullptr) {}
};

/* normal List */
/**
 * @brief: generate a normal linked list
 *
 * @param: vec      list vals in expected order
 * @return:         list head
 */
template <typename T = ListNode>
inline T *generateLinkedList(const vector<int> &vec) {
    if (vec.empty()) return nullptr;
    T *pBeforeHead = new T(-1);
    T *pCurrent = pBeforeHead;
    for (const auto &v : vec) {
        T *pNode = new T(v);
        pCurrent->next = pNode;
        pCurrent = pNode;
    }
    auto *pHead = pBeforeHead->next;
    delete pBeforeHead;
    return pHead;
}

/**
 * @param: release a linked list
 *
 * @param: pHead        head of the list you want to release
 */
template <typename T = ListNode>
inline void releaseLinkedList(T *pHead) {
    auto *pCurrent = pHead;
    while (pCurrent != nullptr) {
        auto *pNext = pCurrent->next;
        delete pCurrent;
        pCurrent = pNext;
    }
}

/**
 * @param: display the linked list
 *
 * @param: pHead        head of the list you want to display
 */
template <typename T = ListNode>
inline void displayLinkedList(T *pHead) {
    auto *p = pHead;
    while (p) {
        std::cout << p->val << " -> ";
        p = p->next;
    }
    std::cout << "nullptr" << std::endl;
}

/* random List */
/**
 * @brief: generate a random list
 *
 * @param: nodes        list vals in expected order
 * @param: randIndices  index of the list node that current node's random pointer points to
 *                      index is starts from 0
 * @return:             list head
 */
template <typename T = RandomListNode>
inline T *generateRandomList(const vector<int> &nodes, const vector<int> &randIndices) {
    T *head = generateLinkedList<T>(nodes);
    if (!head) head;
    T *ptr = head, *cur = head;
    for (const auto &index : randIndices) {
        for (int i = 0; i < index; ++i) {
            ptr = ptr->next;
            if (ptr == nullptr) {
                std::cout << "Error: Inavlid random index!" << std::endl;
                releaseRandomList(head);
                return nullptr;
            }
        }
        if (index == -1) ptr = nullptr;
        cur->random = ptr;
        cur = cur->next;
        ptr = head;
    }
    return head;
}

/**
 * @param: release the random list
 *
 * @param: pHead        head of the list you want to release
 */
template <typename T = RandomListNode>
inline void releaseRandomList(T *head) {
    releaseLinkedList<T>(head);
}

/**
 * @param: display the random list
 *
 * @param: pHead        head of the list you want to display
 *                      node1 (node1's random index) -> node2 (node2's rand idx) -> ... -> nullptr
 */
template <typename T = RandomListNode>
inline void displayRandomList(T *head) {
    auto *pCur = head;
    while (pCur) {
        std::cout << pCur->label << " ( ";
        if (!pCur->random) {
            std::cout << "null ) -> ";
        } else {
            int index = 0;
            for (auto *p = head; p && p != pCur->random; p = p->next) ++index;
            std::cout << index << " ) -> ";
        }
        pCur = pCur->next;
    }
    std::cout << "nullptr" << std::endl;
}

}  // namespace utils
#endif  // __LINKED_LIST_UTIL_HPP__