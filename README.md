<剑指 Offer> 练习

> OJ
https://www.nowcoder.com/ta/coding-interviews

> C++ Solutions
```bash
cd cpp
cmake .
make
./out/xx-xxx-xxx
```

> Python Solutions
```bash
cd py
./xx-xxx-xxx.py
```